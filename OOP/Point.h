#pragma once


class Point {

	int x = 0, y = 0;

public:

	void set_point(int x1, int y1) { x = x1; y = y1; };
	int get_x() { return x; };
	int get_y() { return y; };
	bool is_point_in_range(int max_x, int max_y);
};