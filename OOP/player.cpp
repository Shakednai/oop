#include "player.h"
#include <iostream>
#include <stdio.h>

using namespace std;

bool Player::insert_soldier_to_player(Soldier sol){

	if (index >= max_soldiers) {
		return false;
	}
	soldiers[index] = sol;
	index++;
	return true;
}

int Player::get_num_of_active_moveable_soldiers() {
	
	int count = 0;
	int size = sizeof(soldiers) / sizeof(Soldier);
	for (int i = 0; i < size; i++) {
		TYPES type = soldiers[i].get_type();
		STATUS status = soldiers[i].get_status();
		if (type != TYPES::F && type != TYPES::B && status == STATUS::active) {
			count++;
		}
	}

	return count;

}

int main() {
	Player p1;
	Soldier s;
	s.set_soldier(TYPES::P, 3, 3);
	s.set_status(STATUS::inactive);
	s.set_position(4, 3);
	p1.insert_soldier_to_player(s);
	Soldier * sl = p1.get_soldiers();
	cout << p1.get_num_of_active_moveable_soldiers() << endl;


	return 0;
}