
#include <stdlib.h> 
#include "soldier.h"


bool Soldier::set_position(int x, int y) {
	
	int move_x = abs(position.get_x() - x);
	int move_y = abs(position.get_y() - y);
	Point p;
	p.set_point(x, y);
	bool res = p.is_point_in_range(Rows, Columns);
	//if the step is not valid -
	// if the step is out of board border
	// the step is bigger than one step forword or one step to the side
	if (res) {
		if ((move_x == 1 && move_y == 0) || (move_x == 0 && move_y == 1)) {
			position = p;
			return true;
		}
	}
	return false;

}