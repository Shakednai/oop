#pragma once
#include "Point.h"
#include "Config.h"


class Soldier {

	STATUS stat = inactive;
	bool is_visible = false;
	TYPES type;
	Point position;
	bool is_joker = false;
	Flat_Players belongs_to;


public:

	void set_soldier(TYPES t, int x1, int y1) { type = t; position.set_point(x1, y1); }
	TYPES get_type() { return type; }
	Point get_position() { return position; }
	void set_joker() { is_joker = true; }
	void set_is_visible() { is_visible = true; }
	void set_status(STATUS s) { stat = s; }
	STATUS get_status() { return stat; }
	bool set_position(int x, int y);
	void set_player_to_belong(Flat_Players p) { belongs_to = p; }
	Flat_Players get_belongs_to() { return belongs_to; }	


};